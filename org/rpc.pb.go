// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.14.0
// source: org/rpc.proto

package org

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

var File_org_rpc_proto protoreflect.FileDescriptor

var file_org_rpc_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x6f, 0x72, 0x67, 0x2f, 0x72, 0x70, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x03, 0x6f, 0x72, 0x67, 0x1a, 0x0d, 0x6f, 0x72, 0x67, 0x2f, 0x67, 0x65, 0x74, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x1a, 0x10, 0x6f, 0x72, 0x67, 0x2f, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0x9e, 0x01, 0x0a, 0x03, 0x52, 0x70, 0x63, 0x12, 0x2d, 0x0a,
	0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x12, 0x0f, 0x2e, 0x6f, 0x72, 0x67, 0x2e, 0x47,
	0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x52, 0x65, 0x71, 0x1a, 0x0f, 0x2e, 0x6f, 0x72, 0x67, 0x2e,
	0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x52, 0x73, 0x70, 0x22, 0x00, 0x12, 0x2a, 0x0a, 0x06,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x0e, 0x2e, 0x6f, 0x72, 0x67, 0x2e, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x1a, 0x0e, 0x2e, 0x6f, 0x72, 0x67, 0x2e, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x52, 0x73, 0x70, 0x22, 0x00, 0x12, 0x3c, 0x0a, 0x0c, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x48, 0x6f, 0x6e, 0x6f, 0x72, 0x73, 0x12, 0x14, 0x2e, 0x6f, 0x72, 0x67, 0x2e, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x48, 0x6f, 0x6e, 0x6f, 0x72, 0x73, 0x52, 0x65, 0x71, 0x1a, 0x14,
	0x2e, 0x6f, 0x72, 0x67, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x48, 0x6f, 0x6e, 0x6f, 0x72,
	0x73, 0x52, 0x73, 0x70, 0x22, 0x00, 0x42, 0x19, 0x0a, 0x0c, 0x63, 0x6f, 0x6d, 0x2e, 0x64, 0x65,
	0x61, 0x6e, 0x2e, 0x6f, 0x72, 0x67, 0x50, 0x01, 0x5a, 0x07, 0x6f, 0x72, 0x67, 0x3b, 0x6f, 0x72,
	0x67, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_org_rpc_proto_goTypes = []interface{}{
	(*GetByIdReq)(nil),      // 0: org.GetByIdReq
	(*UpdateReq)(nil),       // 1: org.UpdateReq
	(*UpdateHonorsReq)(nil), // 2: org.UpdateHonorsReq
	(*GetByIdRsp)(nil),      // 3: org.GetByIdRsp
	(*UpdateRsp)(nil),       // 4: org.UpdateRsp
	(*UpdateHonorsRsp)(nil), // 5: org.UpdateHonorsRsp
}
var file_org_rpc_proto_depIdxs = []int32{
	0, // 0: org.Rpc.GetById:input_type -> org.GetByIdReq
	1, // 1: org.Rpc.Update:input_type -> org.UpdateReq
	2, // 2: org.Rpc.UpdateHonors:input_type -> org.UpdateHonorsReq
	3, // 3: org.Rpc.GetById:output_type -> org.GetByIdRsp
	4, // 4: org.Rpc.Update:output_type -> org.UpdateRsp
	5, // 5: org.Rpc.UpdateHonors:output_type -> org.UpdateHonorsRsp
	3, // [3:6] is the sub-list for method output_type
	0, // [0:3] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_org_rpc_proto_init() }
func file_org_rpc_proto_init() {
	if File_org_rpc_proto != nil {
		return
	}
	file_org_get_proto_init()
	file_org_update_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_org_rpc_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_org_rpc_proto_goTypes,
		DependencyIndexes: file_org_rpc_proto_depIdxs,
	}.Build()
	File_org_rpc_proto = out.File
	file_org_rpc_proto_rawDesc = nil
	file_org_rpc_proto_goTypes = nil
	file_org_rpc_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RpcClient is the client API for Rpc service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RpcClient interface {
	// 通过机构编号获取机构信息
	GetById(ctx context.Context, in *GetByIdReq, opts ...grpc.CallOption) (*GetByIdRsp, error)
	// 通过编号上传修改机构扩展信息
	Update(ctx context.Context, in *UpdateReq, opts ...grpc.CallOption) (*UpdateRsp, error)
	// 通过编号修改机构荣誉信息
	UpdateHonors(ctx context.Context, in *UpdateHonorsReq, opts ...grpc.CallOption) (*UpdateHonorsRsp, error)
}

type rpcClient struct {
	cc grpc.ClientConnInterface
}

func NewRpcClient(cc grpc.ClientConnInterface) RpcClient {
	return &rpcClient{cc}
}

func (c *rpcClient) GetById(ctx context.Context, in *GetByIdReq, opts ...grpc.CallOption) (*GetByIdRsp, error) {
	out := new(GetByIdRsp)
	err := c.cc.Invoke(ctx, "/org.Rpc/GetById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcClient) Update(ctx context.Context, in *UpdateReq, opts ...grpc.CallOption) (*UpdateRsp, error) {
	out := new(UpdateRsp)
	err := c.cc.Invoke(ctx, "/org.Rpc/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcClient) UpdateHonors(ctx context.Context, in *UpdateHonorsReq, opts ...grpc.CallOption) (*UpdateHonorsRsp, error) {
	out := new(UpdateHonorsRsp)
	err := c.cc.Invoke(ctx, "/org.Rpc/UpdateHonors", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RpcServer is the server API for Rpc service.
type RpcServer interface {
	// 通过机构编号获取机构信息
	GetById(context.Context, *GetByIdReq) (*GetByIdRsp, error)
	// 通过编号上传修改机构扩展信息
	Update(context.Context, *UpdateReq) (*UpdateRsp, error)
	// 通过编号修改机构荣誉信息
	UpdateHonors(context.Context, *UpdateHonorsReq) (*UpdateHonorsRsp, error)
}

// UnimplementedRpcServer can be embedded to have forward compatible implementations.
type UnimplementedRpcServer struct {
}

func (*UnimplementedRpcServer) GetById(context.Context, *GetByIdReq) (*GetByIdRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (*UnimplementedRpcServer) Update(context.Context, *UpdateReq) (*UpdateRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (*UnimplementedRpcServer) UpdateHonors(context.Context, *UpdateHonorsReq) (*UpdateHonorsRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateHonors not implemented")
}

func RegisterRpcServer(s *grpc.Server, srv RpcServer) {
	s.RegisterService(&_Rpc_serviceDesc, srv)
}

func _Rpc_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/org.Rpc/GetById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).GetById(ctx, req.(*GetByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Rpc_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/org.Rpc/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).Update(ctx, req.(*UpdateReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Rpc_UpdateHonors_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateHonorsReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).UpdateHonors(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/org.Rpc/UpdateHonors",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).UpdateHonors(ctx, req.(*UpdateHonorsReq))
	}
	return interceptor(ctx, in, info, handler)
}

var _Rpc_serviceDesc = grpc.ServiceDesc{
	ServiceName: "org.Rpc",
	HandlerType: (*RpcServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetById",
			Handler:    _Rpc_GetById_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _Rpc_Update_Handler,
		},
		{
			MethodName: "UpdateHonors",
			Handler:    _Rpc_UpdateHonors_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "org/rpc.proto",
}
